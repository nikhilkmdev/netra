from logging import info

from cro.exchange.api.public import PublicAPIClient
from psf.jivrtt.flow.run import Ingress


class CryptoComIngress(Ingress):

    def __init__(self):
        self._client = PublicAPIClient(is_sandbox=False)

    def ingress(self, instrument_names):
        info(f'Got {len(instrument_names)} to fetch price')
