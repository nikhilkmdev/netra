from logging import info

from psf.jivrtt.flow.run import Ingress


class LoggingIngress(Ingress):

    def ingress(self, input_to_process):
        """
        Logs the inputs to process
        :param input_to_process: iter of tasks
        """
        info(f'Ingress got inputs to process: [{input_to_process}]')
