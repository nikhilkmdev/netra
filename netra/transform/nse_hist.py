from psf.jivrtt.flow.base import BaseTransformer


class NSEHistoricalTransformer(BaseTransformer):

    def transform(self, ts_dfs):
        """ The transformation logic for NSE historical data
        :param: ts_df: list of time series data frames
        :return: the transformed standardized data frame
        """
        headers = [
            "Symbol", "Series", "Date", "Prev Close",
            "Open Price", "High Price", "Low Price", "Close Price",
            "Total Traded Quantity",
        ]
        return [ts_df[headers].rename(columns={
            "Symbol": "sym",
            "Series": "inst",
            "Date": "ts",
            "High Price": "high",
            "Low Price": "low",
            "Close Price": "close",
            "Total Traded Quantity": "vol",
        }) for ts_df in ts_dfs]
