from psf.jivrtt.flow.run import Transformer


class CryptoComTransformer(Transformer):

    def transform(self, tickers):
        transformed_tickers = []
        for ticker in tickers:
            base_ccy, quote_ccy = ticker['i'].split('_')
            transformed_tickers.append({
                'sym': ticker['i'],
                'base': base_ccy,
                'quote': quote_ccy,
                'inst': 'crypto',
                'ts': int(ticker['t'] * pow(10, 6)),
                'high': ticker['h'],
                'low': ticker['l'],
                'vol': ticker['v'],
                'prc': ticker['k']
            })
        return transformed_tickers
