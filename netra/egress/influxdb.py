from logging import info

from jivrtt.influx.client import InfluxQLClient
from psf.jivrtt.flow.run import Egress


class InfluxDBEgress(Egress):

    def __init__(self, host, port, db, measure):
        self._db = db
        self._measure = measure
        self._influx_client = InfluxQLClient(host, port=port)
        self._init()

    def _init(self):
        self._influx_client.create_db(self._db)

    def egress(self, tickers):
        for ticker in tickers:
            ts = ticker.pop('ts', None)
            tags = {'sym': ticker.pop('sym')}
            tags.update({'inst': ticker.pop('inst')})
            tags.update({'base': ticker.pop('base')})
            tags.update({'quote': ticker.pop('quote')})
            resp = self._influx_client.write_many_fields(self._db, self._measure, ticker, tags=tags, timestamp=ts)
            info(f'Response from server: {resp}')
