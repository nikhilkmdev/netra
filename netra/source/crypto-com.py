from logging import info
from time import sleep

from cro.exchange.api.public import PublicAPIClient
from psf.jivrtt.flow.run import Source


class CryptoComApiSource(Source):

    def __init__(self):
        self._client = PublicAPIClient(is_sandbox=False)

    def inputs(self):
        """
        Gets the instruments for getting time series
        :return: iter of instrument names
        """
        instruments = self._client.get_instruments()
        instruments = instruments.get('result', {}).get('instruments', [])
        instruments = {inst['instrument_name'] for inst in instruments if inst['quote_currency'] == 'USDT'}
        return list(instruments)

    def read(self, source_ids=None):
        """
        Reads the available tickers
        :param source_ids: if needed to narrow down the list
        :return: list of source ids
        """
        instruments = source_ids or self.inputs()
        return self._get_ticker_data(instruments)

    def _get_ticker_data(self, instrument_names):
        tickers = []
        for instrument_name in instrument_names:
            sleep(0.1)
            info(f'Fetching {instrument_name} price')
            ticker = self._client.get_ticker(instrument_name=instrument_name)
            ticker = ticker.get('result', {}).get('data', {})
            tickers.append(ticker)
        return tickers
