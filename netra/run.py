import argparse
import importlib
import logging
import sys
from logging import info, INFO, error
from os import getcwd
from time import sleep

import yaml
from psf.jivrtt.flow.flow import run_flow


def run_netra(source_system, config_file):
    """
    Runs the sourcing of time series into a format which can be consumed by buddhi
    :param source_system: the source system for which we are fetching the TS
    :param config_file: configuration file with all the details for the run
    """
    source_config = _parse_config_file(config_file)[source_system]
    source = _get_instance(source_config['source'])
    ingress = _get_instance(source_config['ingress'])
    transformer = _get_instance(source_config['transformer'])
    egress = _get_instance(source_config['egress'])
    bucket_size = source_config.get('parallelism', dict()).get('bucket_size', 1)
    max_buckets = source_config.get('parallelism', dict()).get('max_buckets', 1)
    log_dir = source_config.get('log_dir', getcwd())
    log_level = logging.getLevelName(source_config['log_level'])
    tasks = source_config.get('source_ids')
    tasks = tasks or source.inputs()
    info(f'Starting time series sourcing for {len(tasks)} instruments')
    run_flow(
        tasks,
        source,
        ingress,
        transformer,
        egress,
        bucket_size=bucket_size,
        max_buckets=max_buckets,
        log_dir=log_dir,
        log_level=log_level,
    )


def _parse_config_file(config_file):
    with open(config_file, 'r') as config:
        return yaml.load(config)


def _get_instance(handler_config):
    module = importlib.import_module(handler_config['handler']['module'])
    handler_class = getattr(module, handler_config['handler']['class'])
    handler_args = handler_config.get('handler_args', list())
    kwargs = handler_config.get('kwargs', dict())
    return handler_class(*handler_args, **kwargs)


def _parse_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--source", help="Source system name", type=str)
    arg_parser.add_argument("--config", help="Config file with all necessary details", type=str)
    return arg_parser.parse_args(sys.argv[1:])


if __name__ == '__main__':
    logging.basicConfig(level=INFO)
    args = _parse_args()
    while True:
        try:
            run_netra(args.source, args.config)
            sleep(60)
        except Exception as e:
            error('Continue!! Nothing can stop us!', e)
